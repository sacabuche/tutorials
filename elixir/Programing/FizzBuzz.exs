wordy = fn 
    (0, 0, _) -> "FizzBuzz"
    (0, _, _) -> "Fizz"
    (_, 0, _) -> "Buzz"
    (_, _, n) -> inspect(n)
end

Enum.map(10..(10 + 6), fn (n) -> IO.puts wordy.(rem(n, 3), rem(n, 5), n) end)
