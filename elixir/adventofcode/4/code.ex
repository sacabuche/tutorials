defmodule Four do
  def findValue(<<"00000">> <> _, _, val) do
    val - 1
  end

  def findValue(hash, key, val) do
     result = :crypto.md5(key <> to_string(val))
     |> Base.encode16
     findValue(result, key, val + 1)
  end

  def run(key) do
     findValue("x", key, 0)
  end
end


IO.puts Four.run("ckczppom")
