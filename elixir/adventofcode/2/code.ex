defmodule Two do
  
  def calc([l, w, h]) do
      2 * (l*w + l*h + w*h) + l*w
  end 

  def clean("") do [0, 0, 0] end
  def clean(line) do
      String.split(line, "x")
      |> Enum.map(fn(x) -> String.to_integer(x) end)
      |> Enum.sort
  end

  def run(path) do
    File.read!(path)
    |> String.split("\n")
    |> Enum.reduce(0, fn (line, acc) -> acc + (clean(line) |> calc)  end)
  end
end


IO.puts Two.run('input.txt')
