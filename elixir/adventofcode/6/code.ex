defmodule Six do

  def turn_on(key, map) do
    Map.put(map, key, :on)
  end

  def turn_off(key, map) do
    Map.delete(map, key)
  end

  def toogle(key, map) do
    case Map.get(map, key) do
       nil -> turn_on(key, map)
       :off -> turn_on(key, map)
       :on -> turn_off(key, map)
    end
  end

  def executor(action) do
    case action do
      :on -> &turn_on/2
      :off -> &turn_off/2
      :toogle -> &toogle/2
    end
  end

  def get_content(path) do
    File.read!(path)
    |> String.strip
    |> String.split("\n")
  end

  def parse_coordinates(action, coordinates) do
    [start, end_]  = String.split(coordinates, " through ")
    [action | parse_nums(start) ++ parse_nums(end_)]
  end

  def parse_nums(nums) do
    String.split(nums, ",")
    |> Enum.map(&String.to_integer/1)
  end
 
  def parse_line("turn on " <> line) do
    parse_coordinates(:on, line)
  end

  def parse_line("turn off " <> line) do
    parse_coordinates(:off, line)
  end

  def parse_line("toggle " <> line) do
    parse_coordinates(:toogle, line)
  end
  
  def gen_keys([x0, y0, x1, y1]) do
    (for x <- x0..x1, y <- y0..y1, do: {x, y})
  end

  def get_fun([action | coordinates], map) do
    IO.puts("#{action} -> #{inspect(coordinates)} -> #{Map.size(map)}")
    f = executor(action)
    gen_keys(coordinates)
    |> Enum.reduce(map, f)
  end

  def run(path) do
    path
    |> get_content
    |> Enum.map(&parse_line/1)
    |> Enum.reduce(Map.new(), &get_fun/2)
    |> Map.size
    |> IO.puts
  end

end


Six.run("input.txt")
