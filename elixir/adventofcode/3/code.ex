defmodule Three do
  def deliver(data) do
    current = [0, 0]
    deliver(data, MapSet.new([current]), current)
  end

  def deliver(<<v>> <> data, set, [x, y]) do
    case <<v>> do
      "<" -> x = x - 1
      ">" -> x = x + 1
      "^" -> y = y + 1
      "v" -> y = y - 1
      _ -> IO.puts("Error")
    end
    current = [x, y]
    deliver(data, MapSet.put(set, current), current)
  end

  def deliver(_, set, _) do
    MapSet.size(set)
  end

  def run(path) do
    File.read!(path)
    |> deliver
  end
end


IO.puts Three.run("input.txt")
