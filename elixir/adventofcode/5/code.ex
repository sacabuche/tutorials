defmodule Five do

  def is_vowel(letter) do
    case letter in 'aeiou' do
      true -> true
      false -> false
    end
  end

  def count_vowels(text) do
    Enum.filter(to_char_list(text), &is_vowel/1)
    |> length
  end

  def has_forbiden(<<a, b>> <> text) do
     case <<a, b>> in ["ab", "cd", "pq", "xy"] do
       true -> true
       false -> has_forbiden(<<b>> <> text)
     end
  end

  def has_forbiden(_) do
    false
  end

  def has_twice("") do
    false
  end
  
  def has_twice(<<x, x>> <> _) do
     true
  end

  def has_twice(<<_>> <> text) do
     has_twice(text)
  end

  def is_nice(text) do
      has_twice(text) and
      !has_forbiden(text) and
      count_vowels(text) > 2
  end

  def run(path) do
    File.read!(path)
    |> String.split("\n")
    |> Enum.filter(&is_nice/1)
    |> length
   end
end


IO.puts Five.run("input.txt")
