defmodule One do

  defp getContent(path) do
     {:ok, file} = File.read(path)
     file
  end

  def go("(" <> tail, num) do
      go(tail, num + 1)
  end

  def go(")" <> tail, num) do
      go(tail, num - 1)
  end

  def go("" <> tail, num) do
      num
  end

  def run(name) do
    getContent(name)
    |> to_string
    |> go(0)
    |> IO.puts
  end
end
