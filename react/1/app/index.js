var React = require('react');
var ReactDOM = require('react-dom');
var routes = require('./config/routes');

/*
* Focused
* Independent
* Resusable
* Small
* Testable
* */


ReactDOM.render(
  routes,
  document.getElementById('app')
);
